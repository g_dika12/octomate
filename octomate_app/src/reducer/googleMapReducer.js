const initialState = {
    latitude: null,
    longitude: null,
    isLoading: false
}

const googleMapReducer = (prevState = initialState, action) => {
    switch(action.type){
        case "_LOADING":
            return {
                ...prevState,
                isLoading: true,
            }
        case 'LOCAL_COORDINATE':
            return {
                ...prevState,
                latitude: action.lat,
                longitude: action.lng,
                isLoading: false
            }
    }
    return prevState;
}
export default googleMapReducer;