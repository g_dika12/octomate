const initialState = {
    imagePath: "",
    isLoading: false,
}

const selfiePhotoReducer = (prevState = initialState, action) => {
    switch (action.type) {
        case "_LOADING":
            return {
                ...prevState,
                isLoading: true,
            }
        case 'TOOK_PHOTO':
            return {
                ...prevState,
                imagePath: action.path,
                isLoading: false
            }
    }
    return prevState;
}
export default selfiePhotoReducer;