import { combineReducers } from "redux";
import googleMapReducer from "./googleMapReducer";
import loginReducer from "./loginReducer";
import selfiePhotoReducer from "./selfiePhotoReducer";
import timesheetReducer from "./timesheetReducer";

const rootReducer = combineReducers({
    login: loginReducer,
    selfie: selfiePhotoReducer,
    googleMap: googleMapReducer,
    timesheet: timesheetReducer,
})
export default rootReducer;