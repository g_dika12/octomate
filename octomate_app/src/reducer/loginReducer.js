const initialState = {
    isLoading: false,
    userEmail: null,
    userToken: null,
    username: null,
}

const loginReducer = (prevState = initialState, action) => {
    switch (action.type) {
        case "_LOADING":
            return {
                ...prevState,
                isLoading: true,
            }
        case 'LOGIN':
            return {
                ...prevState,
                isLoading: false,
                userEmail: action.id,
                userToken: action.token,
                username: action.name
            }
        case 'RETRIEVE_TOKEN':
            return {
                ...prevState,
                userToken: action.token,
                isLoading: false
            }

        case 'LOGOUT':
            return {
                ...prevState,
                isLoading: false,
                userToken: null,
                userEmail: null
            }
        case "REGISTER":
            return {
                ...prevState,
                isLoading: false,
                userEmail: action.id,
                userToken: action.token,
            };
    }
    return prevState;
}
export default loginReducer;
