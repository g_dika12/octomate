const initialState = {
    timesheetData: {},
    isLoading: false,
}

const timesheetReducer = (prevState = initialState, action) => {
    switch (action.type) {
        case "_LOADING":
            return {
                ...prevState,
                isLoading: true,
            }
        case 'GET_TIMESHEET_DATA':
            return {
                ...prevState,
                timesheetData: action.data,
                isLoading: false
            }
    }
    return prevState;
}
export default timesheetReducer;