import AsyncStorage from "@react-native-async-storage/async-storage";
import axios from "axios";
import { baseURL } from "../view/constants/constants";
import { removeKey, setKey } from "./storeKey";

export const loginAction = (userEmail, userToken, username) => {
    return {
        type: "LOGIN",
        id: userEmail,
        token: userToken,
        isLoading: false,
        name: username,
    }
}

export const loginActionController = (userEmail, password) => {
    return async (dispatch) => {
        let userToken;
        userToken = null;
        // let userToken2;
        // userToken2 = null;
        let name;
        name = null;
        if (userEmail === 'mary5@test.com' && password === '12345678') {
            try {
                dispatch(onLoadingProcess())
                console.log('login action controller run')
                axios.post(baseURL.OCTOMATE_DEV + 'login/email',
                    {
                        "email": userEmail,
                        "password": password
                    }
                )
                    .then(async (result) => {
                        console.log('---+++ > ',JSON.stringify(result, null,2))
                        userToken = result.data[0]["associateAuthToken"]
                        // userToken2 = result.data[0]["associateRefreshTokenState"]
                        name = result.data[0]["associateData"]["bankDetails"]["accountName"]
                        console.log('token 1 = ',userToken)
                        // console.log('token 2 = ',userToken2)
                        setKey('token', userToken) // associate auth
                        // setKey('token2', userToken2) // associate refresh auth
                        dispatch(loginAction(userEmail, userToken, name))
                    })
                    .catch((error) => {
                        console.log('login error - ', JSON.stringify(error.message, null, 2))
                    })
            } catch (err) {
                console.log(err)
            }
        } else {
            alert('The email or password is invalid.')
        }
    }
}

export const onLoadingProcess = () => {
    return {
        type: '_LOADING',
        isLoading: true,
    }
}
export const onLoadingControl = () => {
    return (dispatch) => {
        dispatch(onLoadingProcess())
    }
}

export const retrieveTokenAction = (userToken) => {
    return {
        type: 'RETRIEVE_TOKEN',
        isLoading: false,
        token: userToken
    }
}

export const tokenActionController = (userToken) => {
    return (dispatch) => {
        dispatch(retrieveTokenAction(userToken))
    }
}

export const registerAction = (userEmail, userToken) => {
    return {
        type: 'REGISTER',
        id: userEmail,
        token: userToken
    }
}

export const registerController = (email, token) => {
    return (dispatch) => {
        dispatch(registerAction(email, token))
    }
}

export const logoutAction = () => {
    return {
        type: "LOGOUT",
        isLoading: false,
        userToken: null,
        userEmail: null
    }
}

export const logoutController = () => {
    console.log('logout controller run')
    return async (dispatch) => {
        try {
            await removeKey('token')
            console.log('remove token ')
        } catch (error) {
            console.log('--?>', error)
        }
        dispatch(logoutAction())
    }
};