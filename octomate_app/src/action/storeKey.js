import AsyncStorage from "@react-native-async-storage/async-storage";

export const getKey = async (key) => {
    let token = '';
    try {
        token = await AsyncStorage.getItem(`${key}`)
    } catch (error) {
        console.log(error.message)
    }
}

export const setKey = async (key, value) => {
    try {
        console.log('set key run')
        await AsyncStorage.setItem(key, value)
        console.log('store key success')
    } catch (error) {
        console.log('error set key --?> ', error)
    }

}

export const removeKey = async (key) => {
    try {
        await AsyncStorage.removeItem(key)
        console.log('key removed')
    } catch (error) {
        console.log(error.message)
    }
}