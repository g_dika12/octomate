import axios from "axios";
import { baseURL } from "../view/constants/constants";

export const timesheetAction = (get_data) => {

    return {
        type: "GET_TIMESHEET_DATA",
        data: get_data
    }
}

export const timesheetController = async(token) => {
    console.log('tun -> ', token)
    const AuthStr = 'Bearer '.concat(token)
    console.log('==', AuthStr)
    const response = await axios.get(baseURL.TIMESHEET_DEV + 'timesheet',
        {
            headers: {
                "X-AUTH-TOKEN": `${AuthStr}`,
            }
        }
    )
    console.log('->>>__>> ',JSON.stringify(response, null, 2)) 
}

export const onLoadingTimesheet = () => {
    return {
        type: '_LOADING',
        isLoading: true,
    }
}
export const onLoadTImesheetControl = () => {
    return (dispatch) => {
        dispatch(onLoadingTimesheet())
    }
}