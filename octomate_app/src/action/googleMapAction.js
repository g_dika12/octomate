export const googleMapAction=(latitude, longitude)=> {
    return {
        type:"LOCAL_COORDINATE",
        lat: latitude,
        lng: longitude,
        isLoading: false
    }
}

export const googleMapController = (latitude, longitude)=>{
    return async (dispatch)=> {
        if(latitude || longitude !== null){
            try{
                dispatch(googleMapAction(latitude, longitude))
            } catch (error){
                console.log('googleMapController ', error.message)
            }
        }
    }
}