
export const selfieAction = (imgPath) => {
    return {
        type: "TOOK_PHOTO",
        path: imgPath,
        isLoading: false
    }
}

export const selfieActionController = (imagePath) => {
    return async (dispatch) => {
        if (imagePath !== null) {
            try {
                // dispatch(onLoadingControl()); // 
                dispatch(selfieAction(imagePath))
            } catch (error) {
                console.log('selfieActionController - ', error.message)
            }
        }
    }
}

export const onLoadingProcess = () => {
    return {
        type: '_LOADING',
        isLoading: true,
    }
}
export const onLoadingControl = () => {
    return (dispatch) => {
        dispatch(onLoadingProcess())
    }
}