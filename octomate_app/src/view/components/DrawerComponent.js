import { DrawerItem } from '@react-navigation/drawer';
import React from 'react'
import { View, Text } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { constantColor } from '../constants/constantColor';

const DrawerComponent = (props) => {
    const {icon_name,title,onPress} = props;
    return (
        <DrawerItem
            icon={()=>(
                <Icon
                    name={icon_name}
                    color={constantColor.primary}
                    size={30}
                />
                )}
                label={title}
                onPress={()=>onPress()}
        />
    )
}

export default DrawerComponent
