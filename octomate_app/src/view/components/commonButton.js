import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { constantColor } from '../constants/constantColor';
import { globalStyles } from '../constants/globalStyles';

const CommonButton = (props) => {
    const { title, onPress, propStyle } = props;

    return (
        <TouchableOpacity
            onPress={() => onPress()}
            style={propStyle ? propStyle : styles.btn}
        >
            <Text style={[globalStyles.font16B, { color: constantColor.white }]}>{title}</Text>
        </TouchableOpacity>
    )
}

export default CommonButton

const styles = StyleSheet.create({
    btn: {
        backgroundColor: constantColor.primary,
        height: 40,
        width:'100%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius:8,
    }
})
