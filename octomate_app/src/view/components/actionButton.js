import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import { constantColor } from '../constants/constantColor';
import { globalStyles } from '../constants/globalStyles';

const ActionButton = (props) => {
    const { title, onPress, propStyle, textStyle } = props;
    return (
        <TouchableOpacity
            onPress={() => onPress()}
            style={propStyle ? propStyle : globalStyles.actionButton}
        >
            <Text style={[globalStyles.font16B, { color: textStyle?textStyle: constantColor.primary }]}>{title}</Text>
        </TouchableOpacity>
    )
}

export default ActionButton;
