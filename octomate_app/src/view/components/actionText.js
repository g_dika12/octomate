import React from 'react'
import { Text, TouchableOpacity } from 'react-native'
import { constantColor } from '../constants/constantColor'
import { globalStyles } from '../constants/globalStyles'

const ActionText = (props) => {
    const { title, onPress } = props
    return (
        <TouchableOpacity
            onPress={()=>onPress()}
        >
            <Text style={[globalStyles.font18R, { color: constantColor.white, textDecorationLine:'underline' }]}>{title}</Text>
        </TouchableOpacity>
    )
}

export default ActionText
