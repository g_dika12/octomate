import React, { useEffect, useState } from 'react'
import { Text, View, StyleSheet, PermissionsAndroid, Platform } from 'react-native'
import { globalStyles } from '../constants/globalStyles'
import MapView, { Callout, Marker, PROVIDER_GOOGLE } from 'react-native-maps'
import Geolocation from '@react-native-community/geolocation'
import { constantColor } from '../constants/constantColor'
import { useDispatch } from 'react-redux'
import { googleMapController } from '../../action/googleMapAction'

const GoogleMap = () => {
    const dispatch = useDispatch()
    const [initialPosition, setInitialPosition] = useState({
        latitude: -6.902308927614365,
        longitude: 107.61881578608507,
        latitudeDelta: 0.0045,
        longitudeDelta: 0.0045,
    })
    useEffect(() => {
        const requestPositionPermission = async () => {
            try {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                    {
                        title: "Location Permission",
                        message:
                            "Octomate-app need your location permission",
                        buttonNeutral: "Ask Me Later",
                        buttonNegative: "Cancel",
                        buttonPositive: "OK"
                    }
                )
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    console.log("Geo location granted");
                } else {
                    console.log("Geo location denied");
                }

            } catch (error) {
                console.log(error.message)
            }
        }
        if (Platform === 'android') { return requestPositionPermission() }

        Geolocation.getCurrentPosition(
            info => {
                const latitude = info["coords"]["latitude"];
                const longitude = info["coords"]["longitude"];
                    setInitialPosition({
                        ...initialPosition,
                        latitude,
                        longitude,
                    })
                    dispatch(googleMapController(latitude, longitude))
            }
        )

    }, [])
    return (
        <View style={styles.container}>
            <MapView
                provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                style={styles.map}
                region={initialPosition}
            >
                <Marker
                    coordinate={{ latitude: initialPosition.latitude, longitude: initialPosition.longitude }} style={{ width: 20, height: 20 }}
                    image={require('../assets/png/g-map48.png')}
                >
                    <Callout tooltip>
                        <View style={[styles.bubble, globalStyles.center]}>
                            <Text style={globalStyles.font14B}>Your current location</Text>
                        </View>
                    </Callout>
                </Marker>
            </MapView>
        </View>
    )
}

export default GoogleMap

const styles = StyleSheet.create({
    container: {
        // ...StyleSheet.absoluteFillObject,
        height: 300,
        width: '100%',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    bubble: {
        width: 200,
        height: 36,
        borderWidth: 1,
        borderColor: constantColor.primary,
        borderRadius: 8,
        alignItems: 'center'
    },
    box: {
        height: 45,
        width: '100%',
        alignItems: 'center',
        backgroundColor: constantColor.turqoise
    }
});