export const constantColor = {
    primary: '#5cb8b2', //'rgba(92, 184, 178)',
    black: '#000000',//'rgba(0, 0, 0, 0.1)',
    white:'#ffffff',//'rgba(255, 255, 255, 1)'
    red:"#C83C1E",
    turqoise:'#bce7e4',
    gray:"#dcdee0",
    paleCream:'#f0f4f5',
}