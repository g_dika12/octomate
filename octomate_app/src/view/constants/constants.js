import { Dimensions, Platform } from 'react-native';

const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;

export const height = screenHeight < screenWidth ? screenWidth : screenHeight;
export const width = screenWidth < screenHeight ? screenWidth : screenHeight;

export const defaultFont = 'NunitoSans-Regular';
export const defaultFontBold = 'NunitoSans-Bold';

export const baseURL = { 
    OCTOMATE_DEV: 'https://dev.octomate.us/api/',
    TIMESHEET_DEV: 'https://api-dev.octomate.us/',
}