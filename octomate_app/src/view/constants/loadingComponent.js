import React from 'react'
import { ActivityIndicator, StyleSheet, Text, View } from 'react-native'
import { constantColor } from './constantColor'
import { globalStyles } from './globalStyles'

const LoadingComponent = () => {
    return (
        <View style={[globalStyles.centerContainer, { backgroundColor: constantColor.primary }]}>
            <ActivityIndicator size="large" color={constantColor.black} />
        </View>
    )
}

export default LoadingComponent;

