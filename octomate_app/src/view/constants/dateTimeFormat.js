const date = new Date()
let dd = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();// 17 tgl
let mm = (date.getMonth() + 1) < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1// 5
let yyyy = date.getFullYear() 
let getHour = date.getHours() < 10 ? "0" + date.getHours() : date.getHours(); 
let getMinute = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes()
let getSecond = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds()
let am_pm = date.getHours() >= 12 ? "PM" : "AM";

export const _day = () => {
    return yyyy + '-' + mm + '-' + dd
}
export const _time = () => {
    return getHour + ':' + getMinute + ':' + getSecond + ' ' + am_pm
}
export const _currentTime=()=>{
    return yyyy + '-' + mm + '-' + dd + ' ' + getHour + ':' + getMinute + ':' + getSecond + ' ' + am_pm
}
