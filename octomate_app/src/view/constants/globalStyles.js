import { StyleSheet } from "react-native";
import { constantColor } from "./constantColor";
import { defaultFont, defaultFontBold } from "./constants";

export const globalStyles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: constantColor.primary,
        padding: 16
    },
    mainContainer:{
        flex: 1,
        backgroundColor: constantColor.white,
        padding: 16
    },
    center: {
        justifyContent: 'center',
        alignSelf: 'center'
    },
    centerContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    actionButton: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: constantColor.white,
        width: '100%',
        minHeight: 44,
        borderRadius: 8,
        margin: 16
    },
    selfCenter: {
        justifyContent: 'center',
        alignSelf: 'center'
    },
    //
    font24R: {
        fontFamily: defaultFont,
        fontSize: 24,
    },
    font24B: {
        fontFamily: defaultFontBold,
        fontSize: 24,
    },
    font18R: {
        fontFamily: defaultFont,
        fontSize: 18,
    },
    font18B: {
        fontFamily: defaultFontBold,
        fontSize: 18,
    },
    font16R: {
        fontFamily: defaultFont,
        fontSize: 16,
    },
    font16B: {
        fontFamily: defaultFontBold,
        fontSize: 16,
    },
    font15R: {
        fontFamily: defaultFont,
        fontSize: 15,
    },
    font15B: {
        fontFamily: defaultFontBold,
        fontSize: 15,
    },
    font14R: {
        fontFamily: defaultFont,
        fontSize: 14,
    },
    font14B: {
        fontFamily: defaultFontBold,
        fontSize: 14,
    },
    font12R: {
        fontFamily: defaultFont,
        fontSize: 12,
    },
    font12B: {
        fontFamily: defaultFontBold,
        fontSize: 12,
    },
})