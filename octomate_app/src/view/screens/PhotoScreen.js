import React, { useState } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import { useDispatch } from 'react-redux';
import { selfieActionController } from '../../action/selfiePhotoAction';
import { constantColor } from '../constants/constantColor';
import { globalStyles } from '../constants/globalStyles';

const PhotoScreen = (props) => {
    const { navigation } = props;
    const [imgPath, setImgPath] = useState('');
    const dispatch = useDispatch();
    const takePhotoFromCamera = () => {
        ImagePicker.openCamera({
            width: 300,
            height: 400,
            cropping: true,
        })
        .then(image => {
            setImgPath(image['path'])
            dispatch(selfieActionController(image['path']))
            navigation.navigate('timeIn')
        })
        
    }

    return (
        <View style={globalStyles.container}>
            <TouchableOpacity
                style={[globalStyles.actionButton, { margin: 0, backgroundColor: constantColor.turqoise }]}
                onPress={() => { takePhotoFromCamera() }}>
                <Text style={globalStyles.font16B}>Please take a selfie</Text>
            </TouchableOpacity>

        </View>
    )
}

export default PhotoScreen
