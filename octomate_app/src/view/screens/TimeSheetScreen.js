import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { useEffect, useLayoutEffect, useState } from 'react';
import { Alert, FlatList, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import { Calendar } from 'react-native-calendars';
import Icon from 'react-native-vector-icons/Ionicons';
import { timesheetController } from '../../action/timesheetAction';
import { constantColor } from '../constants/constantColor';
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import { globalStyles } from '../constants/globalStyles';

const TimeSheetScreen = (props) => {
    const { navigation } = props;
    const [user, setUser] = useState('');
    const [user2, setUser2] = useState('')
    const [isLoading, setIsLoading] = useState(false)

    useEffect(() => {
        fetchUser()
    }, [])

    const fetchUser = async () => {
        setIsLoading(true)
        const userData = await AsyncStorage.getItem('token');
        setUser(userData);
        const userData2 = await AsyncStorage.getItem('token2');
        setUser2(userData2);
        setIsLoading(false)
    }

    useLayoutEffect(() => {
        navigation.setOptions({
            headerLeft: () => (
                <Icon
                    name="menu"
                    size={30}
                    color={constantColor.white}
                    onPress={() => navigation.openDrawer()}
                    style={{ marginLeft: 8 }}
                />
            )
        })
    }, []);

    const markingDay = (day) => {
        Alert.alert(
            'Do you want to mark this day ?',
            'The date will be marked',
            [
                {
                    text: 'Okay',
                    onPress: () => {
                        setSelectedDay(day["dateString"]),
                            // console.log('===== ',user)
                        timesheetController(user)
                    },
                },
                {
                    text: 'Cancel',
                    onPress: () => { console.log("Cancel Pressed") },
                }
            ]
        )
    }

    const [selectedDay, setSelectedDay] = useState("")

    if (selectedDay !== '') {
        var days = ''
        days = selectedDay.toString();
        var dayData2 = { [days]: { selected: true } }
        console.log(dayData2)
    }
    const dummyAssociate = [
        {
            id: 0,
            title: 'Another Associate',
            time: '05.00 PM to 07.00 PM',
            hours: '02H 00M',
            break: '00H 00M',
            btn: 'Submitted',
            color: "#f5e6cb"
        },
        {
            id: 1,
            title: 'Sin8 Associate',
            time: '07.00 AM to 02.00 PM',
            hours: '07H 00M',
            break: '01H 00M',
            btn: 'Approved',
            color: '#c8d6ab'
        },
        {
            id: 2,
            title: 'Sin8 Associate',
            time: '07.00 AM to 00.00 PM',
            hours: '03H 00M',
            break: '01H 00M',
            btn: "Rejected",
            color: '#a88377'
        },
    ]
    const renderAssociate = ({ item }) => (
        <View style={styles.box}>
            <View style={{ flexDirection: 'row', justifyContent:'space-between', marginBottom:16 }}>
                <View>
                    <Text style={[globalStyles.font15B, { color: constantColor.primary, marginBottom: 8 }]}>
                        {item.title}
                    </Text>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={globalStyles.font14B}>Time: </Text>
                        <Text style={globalStyles.font14R}>{item.time} </Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={globalStyles.font14B}>Total Hours: </Text>
                        <Text style={globalStyles.font14R}>{item.hours} </Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={globalStyles.font14B}>Break: </Text>
                        <Text style={globalStyles.font14R}>{item.break} </Text>
                    </View>
                </View>
                <TouchableOpacity style={[styles.btn2, { backgroundColor: `${item.color}` }]}>
                    <Text style={globalStyles.font15B}>{item.btn}</Text>
                </TouchableOpacity>

            </View>
                <View style={styles.line} />
        </View>
    )
    return (
        <ScrollView showsVerticalScrollIndicator={false}>
            <Calendar
                minDate={'2018-01-01'}
                maxDate={'2022-12-31'}
                onDayLongPress={(day) => { markingDay(day) }}
                hideExtraDays={true}
                markedDates={dayData2}
                markingType={'multi-period'}
            />
            <View style={{ paddingHorizontal: 8, marginBottom: 44, backgroundColor:constantColor.paleCream }}>
                <View style={{ flexDirection: 'row', marginTop: 8 }}>
                    <Icon
                        name="alert-circle-outline"
                        size={20}
                        color={constantColor.black}
                        onPress={() => navigation.openDrawer()}
                        style={{ marginRight: 8 }}
                    />
                    <Text style={[globalStyles.font14R, { color: constantColor.primary }]}>
                        Tip: Tap and hold the date to add multiple timesheets.
                    </Text>
                </View>
                <View>
                    <Text style={[globalStyles.font12R, { color: constantColor.primary, marginTop: 8 }]}>Fri 22</Text>
                    <Text style={globalStyles.font14B}>Announcement</Text>
                    <View style={styles.line} />
                </View>
                <FlatList
                    data={dummyAssociate}
                    renderItem={renderAssociate}
                    keyExtractor={item => item.id}
                />

            </View>
        </ScrollView>
    )
}

export default TimeSheetScreen;

const styles = StyleSheet.create({
    line: {
        width: '100%',
        // borderWidth: 0.6,
        height:0.6,
        backgroundColor:constantColor.gray,
        marginVertical: 8
    },
    box: {
        width: '100%',
        maxWidth: '100%',
    },
    card2: {
        padding: 8,
        backgroundColor: '#fff',
        maxWidth: "100%",
        borderRadius: 8,
        marginBottom: 12
    },

    btn2: {
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 8,
        width: 84,
        height: 32,
        marginHorizontal: 6,
        marginTop: 12,
        // backgroundColor: constantColor.primary 
    }

})