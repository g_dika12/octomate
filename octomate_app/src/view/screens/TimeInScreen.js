import React, { useEffect, useState } from 'react'
import { Modal, StyleSheet, Text, TextInput, View } from 'react-native'
import Geocoder from 'react-native-geocoding'
import Icon from 'react-native-vector-icons/Ionicons'
import { useSelector } from 'react-redux'
import ActionButton from '../components/actionButton'
import CommonButton from '../components/commonButton'
import GoogleMap from '../components/googleMap'
import { constantColor } from '../constants/constantColor'
import { _currentTime, _time } from '../constants/dateTimeFormat'
import { globalStyles } from '../constants/globalStyles'
import CheckBox from '@react-native-community/checkbox';

const TimeInScreen = (props) => {
    const { navigation } = props;
    const [toggleCheckBox, setToggleCheckBox] = useState(false)
    const [data, setData] = useState({
        visible: false,
        map_time: '',
        modal_time: '',
    })
    const { latitude, longitude } = useSelector(state => ({
        latitude: state.googleMap.latitude,
        longitude: state.googleMap.longitude,
    }))
    const getLocation = () => {
        Geocoder.init("AIzaSyBaq7KQWyWgcd2O0bp81pltvjAnpWZ-yiY");
        Geocoder.from(latitude, longitude)
            .then(json => {
                var addressComponent = json.results[0].address_components[0];
                console.log(addressComponent);
            })
            .catch(error => console.warn(error))
    }

    // console.log(getLocation())// BILL PAYMENT IN GOOGLE DEVELOPER

    useEffect(() => {
        updateTime()
    }, [])

    const updateTime = () => {
        const date = new Date()
        let monthShort = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
        let dd = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();// 17 tgl
        let mmNumber = (date.getMonth() + 1) < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1// 5
        let month = monthShort[date.getMonth()]
        let yyyy = date.getFullYear()
        let getHour = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
        let getMinute = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes()
        let getSecond = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds()
        let am_pm = date.getHours() >= 12 ? "PM" : "AM";

        const updatedTime = getHour + ':' + getMinute + ':' + getSecond + ' ' + am_pm;
        const modal_time = getHour + ':' + getMinute + ', ' + dd + ' ' + month + ' ' + yyyy
        setData({
            ...data,
            map_time: updatedTime.toString(),
            modal_time
        })
    }

    const onConfirmationTime = () => {
        setData({
            ...data,
            visible: true,
        })
    }

    return (
        <View style={[globalStyles.mainContainer, { padding: 0 }]}>
            <View style={[styles.box, globalStyles.center]}>
                <Text style={globalStyles.font14B}>Current location, accuracy 14 meter(s) </Text>
            </View>
            <GoogleMap />
            <View style={[globalStyles.center, { marginTop: 24 }]}>
                <Text style={globalStyles.font16B}>Your Location ...</Text>
                <Text style={[globalStyles.font16B, { color: constantColor.primary, marginTop: 4 }]}>{data.map_time}</Text>
            </View>
            <View style={styles.button}>
                <CommonButton
                    title="Confirm Time In"
                    onPress={onConfirmationTime}
                />
            </View>
            <View style={styles.centeredView}>
                <Modal
                    animationType='slide'
                    transparent={true}
                    visible={data.visible}
                >
                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>
                            <Icon style={{ alignSelf: 'center', marginBottom: 8 }} name="checkmark-circle-outline" size={30} color={constantColor.primary} />
                            <Text style={[globalStyles.font16B, { alignSelf: 'center' }]}>Time In Success</Text>
                            <Text style={[globalStyles.font14R], { alignSelf: 'center', marginBottom: 8 }}>{data.modal_time}</Text>

                            <Text style={globalStyles.font15R}>Enter remarks below, if any</Text>
                            <View style={styles.inputBox}>
                                <TextInput
                                    placeholder='Type Remarks'
                                    multiline={true}
                                />
                            </View>
                            <View style={{ flexDirection: 'row', paddingRight: 8, marginTop: 8}}>
                                <CheckBox
                                    disabled={false}
                                    value={toggleCheckBox}
                                    onValueChange={(newValue) => setToggleCheckBox(newValue)}
                                    tintColor={constantColor.gray}
                                    onTintColor={constantColor.primary}
                                    onCheckColor={constantColor.primary}
                                    boxType={'square'}
                                    style={{ width: 24, height: 24 }}
                                />
                                <Text style={[globalStyles.font14R, { color: constantColor.red, marginHorizontal:8 }]}>
                                    Please tick this box if you have an approved extension from your manager or it will not be paid out
                                </Text>
                            </View>
                            <View style={{ alignSelf: 'center', }}>
                                <ActionButton
                                    title={'GO BACK TO HOME PAGE'}
                                    onPress={() => {
                                        setData({ ...data, visible: false })
                                        navigation.navigate('profile')
                                    }}
                                />
                            </View>
                        </View>
                    </View>
                </Modal>
            </View>
        </View>
    )
}

export default TimeInScreen;

const styles = StyleSheet.create({
    container: {
        // ...StyleSheet.absoluteFillObject,
        height: 300,
        width: '100%',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    bubble: {
        width: 200,
        height: 36,
        borderWidth: 1,
        borderColor: constantColor.primary,
        borderRadius: 8,
        alignItems: 'center'
    },
    box: {
        height: 45,
        width: '100%',
        alignItems: 'center',
        backgroundColor: constantColor.turqoise
    },
    button: {
        marginHorizontal: 32,
        marginVertical: 16
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        // marginTop: 22
    },
    modalView: {
        width: 330,
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 24,
        paddingBottom: 0,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 7,
    },
    inputBox: {
        height: 64,
        backgroundColor: constantColor.gray,
        borderRadius: 8,
        marginTop: 8
    }
});