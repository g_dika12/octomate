import React, { useLayoutEffect } from 'react'
import { View, Text, Button, StyleSheet, Image, TouchableOpacity, ScrollView, FlatList } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import Ionicon from 'react-native-vector-icons/Ionicons'
import { useSelector } from 'react-redux'
import ActionButton from '../components/actionButton'
import { constantColor } from '../constants/constantColor'
import { globalStyles } from '../constants/globalStyles'

const ProfileScreen = (props) => {
    const { navigation } = props;
    const { accountName, profilePhoto } = useSelector((state) => ({
        accountName: state.login.username,
        profilePhoto: state.selfie.imagePath,
    }))
    useLayoutEffect(() => {
        navigation.setOptions({
            headerLeft: () => (
                <Icon
                    name="menu"
                    size={30}
                    color={constantColor.white}
                    onPress={() => navigation.openDrawer()}
                    style={{ marginLeft: 8 }}
                />
            )
        });
    }, [navigation]);
    const dummyData = [
        {
            id: 0,
            head: "Adecco",
            title: "A warm welcome onboard",
            time: "18/05/2021",
            text: "Hi all, here is a quick onboarding document to get you started"
        },
        {
            id: 1,
            head: "Adecco",
            title: "A warm welcome onboard",
            time: "18/05/2021",
            text: "Hi all, here is a quick onboarding document to get you started"
        },
        {
            id: 2,
            head: "Adecco",
            title: "A warm welcome onboard",
            time: "18/05/2021",
            text: "Hi all, here is a quick onboarding document to get you started"
        }
    ];
    const reminderDummy = [
        {
            id: 0,
            title: "Your timesheet has not been submitted!",
            placement: "Placement 090321 (IBM Singapore)",
            timeDif: "From 12 Jan 2018 to 15 Dec 2018"
        },
        {
            id: 1,
            title: "Your timesheet has not been submitted!",
            placement: "Placement 090321 (IBM Singapore)",
            timeDif: "From 12 Jan 2018 to 15 Dec 2018"
        }
    ]

    const renderItem = ({ item, id }) => (
        <View style={styles.card}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 8 }}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ width: 12, height: 12, borderRadius: 6, marginRight: 8, marginTop: 4, backgroundColor: constantColor.red }} />
                    <Text style={globalStyles.font15B}>{item.head}</Text>
                </View>
                <Text style={[globalStyles.font14R, { color: 'gray' }]}>{item.time}</Text>
            </View>

            <Text style={[globalStyles.font15B, { color: constantColor.primary, marginBottom: 8 }]}>{item.title}</Text>
            <Text style={[globalStyles.font15R, { marginBottom: 12 }]}>{item.text}</Text>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 4 }}>
                <TouchableOpacity style={styles.btn}>
                    <Text style={globalStyles.font15B}>General</Text>
                </TouchableOpacity>
                <Text style={[globalStyles.font14R, { marginVertical:4, color: constantColor.primary }]}>Read More</Text>
            </View>
        </View>
    )
    const renderReminder = ({ item, id }) => (
        <View style={styles.card2}>
            <Text style={[globalStyles.font14B, { marginBottom: 8 }]}>{item.title}</Text>
            <View style={{ flexDirection: 'row', justifyContent:'space-between' }}>
                <View>
                    <Text style={[globalStyles.font14R, { marginBottom: 8 }]}>{item.placement}</Text>
                    <Text style={[globalStyles.font14R, { marginBottom: 8 }]}>{item.timeDif}</Text>
                </View>
                <TouchableOpacity style={[styles.btn,styles.btn2]}>
                    <Text style={globalStyles.font15B}>Submit</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
    return (
        <ScrollView 
        keyboardShouldPersistTaps='always'
        keyboardShouldPersistTaps='handled'
         showsVerticalScrollIndicator={false} style={styles.container}>
            <View style={styles.topView}>
                <Image
                    source={profilePhoto === '' ?
                        require('../assets/png/person.jpg') :
                        { uri: profilePhoto }
                    }
                    style={styles.jpg}
                />
                <View>
                    <Text style={globalStyles.font18B}>{accountName}</Text>
                    <ActionButton
                        title='Clock In'
                        onPress={() => { navigation.navigate('photo') }}
                    />
                </View>
            </View>
            <View style={styles.restContainer}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 16 }}>
                    <View style={{ flexDirection: 'row', }}>
                        <Icon
                            name="bullhorn-outline"
                            size={24}
                            color={constantColor.black}
                            style={{ marginRight: 16 }}
                        />
                        <Text style={globalStyles.font18B}>Announcement</Text>
                    </View>
                    <TouchableOpacity>
                        <Text style={[globalStyles.font15B, { color: constantColor.primary }]}>View All</Text>
                    </TouchableOpacity>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: '100%' }}>
                    <FlatList
                        horizontal
                        data={dummyData}
                        keyExtractor={item => item.id}
                        renderItem={(item) => renderItem(item)}
                    />
                </View>
                <View style={{ flexDirection: 'row',marginBottom:12 }}>
                    <Ionicon
                        name="mail-unread-outline"
                        size={24}
                        color={constantColor.primary}
                        style={{ marginRight: 16 }}
                    />
                    <Text style={globalStyles.font18B}>Reminders</Text>
                </View>
                <View style={{marginBottom:40}}>
                    <FlatList
                        data={reminderDummy}
                        keyExtractor={item => item.id}
                        renderItem={(item) => renderReminder(item)}
                    />

                </View>

            </View>
        </ScrollView>
    )
}

export default ProfileScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: constantColor.white
    },
    topView: {
        flexDirection: 'row',
        width: '100%',
        height: 130,
        backgroundColor: constantColor.turqoise,
        padding: 16
    },
    jpg: {
        marginRight: 24,
        width: 100,
        height: 100,
        borderRadius: 50
    },
    restContainer: {
        flex: 1,
        backgroundColor: constantColor.paleCream,
        paddingHorizontal: 16,
        paddingVertical: 8
    },
    card: { padding: 8, backgroundColor: '#fff', maxWidth: 320, borderRadius: 8, margin: 8 },
    card2: { padding: 8, backgroundColor: '#fff', maxWidth: "100%", borderRadius: 8, marginBottom:12 },
    btn: {
        backgroundColor: constantColor.gray,
        width: 84,
        height: 32,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 8
    },
    btn2: { height:28,marginHorizontal:6,marginTop:12, backgroundColor: constantColor.primary }
})