import React, { useState } from 'react'
import { Image, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'
import { useDispatch } from 'react-redux'
import { loginActionController } from '../../action/loginAction'
import ActionButton from '../components/actionButton'
import ActionText from '../components/actionText'
import { constantColor } from '../constants/constantColor'
import { globalStyles } from '../constants/globalStyles'

const LoginScreen = () => {
    const dispatch = useDispatch()
    const [userData, setUserData] = useState({
        email: "",
        password: "",
        visible: true,
    })

    const onIconPressed = () => {
        setUserData({
            ...userData,
            visible: !userData.visible
        })
    }
    const onLoginPressed = (email, password) => {
        dispatch(loginActionController(email, password))
    }
    const onForgotPassword = () => {
        console.log('forgot pressed')
    }

    return (
        <View style={globalStyles.container}>
            <ScrollView showsVerticalScrollIndicator={false}>
                <Text style={[styles.title, globalStyles.font24B, globalStyles.center]}>Octomate</Text>
                <Text style={[globalStyles.font16B, styles.text]}>Email address</Text>
                <TextInput
                    keyboardType='email-address'
                    placeholderTextColor={constantColor.gray}
                    autoCapitalize='none'
                    placeholder="mary5@test.com"
                    style={[globalStyles.font18R, styles.text, { marginBottom: 36 }]}
                    borderBottomWidth={1.165}
                    borderBottomColor={constantColor.white}
                    onChangeText={(val) => setUserData({ ...userData, email: val })}
                />
                <Text style={[globalStyles.font16B, styles.text]}>Password</Text>
                <View style={styles.viewPassword}>
                    <TextInput
                        placeholderTextColor={constantColor.gray}
                        autoCapitalize='none'
                        placeholder="12345678"
                        style={[globalStyles.font18R, styles.text, { marginBottom: 4 }]}
                        onChangeText={(val) => setUserData({ ...userData, password: val })}
                        secureTextEntry={userData.visible}
                    />
                    <TouchableOpacity
                        onPress={() => onIconPressed()}
                        style={{ width: 50, height: 24, alignItems: 'flex-end', }}
                    >
                        <Image
                            source={userData.visible ?
                                require('../assets/png/eye-off-outline.png')
                                :
                                require('../assets/png/eye-outline.png')
                            }
                            style={styles.png}
                        />
                    </TouchableOpacity>
                </View>
                <View style={{ marginBottom: 64 }}>
                    <ActionButton
                        title='Login'
                        onPress={() => onLoginPressed(userData.email, userData.password)}
                        propStyle={[globalStyles.actionButton, globalStyles.selfCenter]}
                    />
                </View>
                <View style={globalStyles.selfCenter}>
                    <ActionText
                        title='Forgot Password?'
                        onPress={onForgotPassword}
                    />
                </View>
            </ScrollView>
        </View>
    )
}

export default LoginScreen
const styles = StyleSheet.create({
    title: {
        color: constantColor.white,
        marginVertical: 40,
    },
    text: {
        color: constantColor.white,
        marginBottom: 16
    },
    png: {
        width: 22,
        height: 22,
    },
    viewPassword: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 40,
        borderBottomColor: constantColor.white,
        borderBottomWidth: 1
    }
})
