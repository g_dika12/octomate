import React from 'react'
import { View, Text, Button } from 'react-native'
import { globalStyles } from '../constants/globalStyles'

const AssignmentScreen = (props) => {
    const {navigation} = props;
    return (
        <View style={globalStyles.centerContainer}>
            <Text>AssignmentScreen</Text>
            <Button title="Open drawer" onPress={() => navigation.openDrawer()} />

        </View>
    )
}

export default AssignmentScreen
