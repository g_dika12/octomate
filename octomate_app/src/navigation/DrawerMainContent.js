import { DrawerContentScrollView, DrawerItem } from '@react-navigation/drawer';
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { useDispatch } from 'react-redux';
import { logoutController } from '../action/loginAction';
import DrawerComponent from '../view/components/DrawerComponent';
import { constantColor } from '../view/constants/constantColor';

const DrawerMainContent = (props) => {
    const { navigation } = props;
    const dispatch = useDispatch()
    return (
        <View style={{ flex: 1 }}>
            <DrawerContentScrollView>
                <DrawerComponent
                    icon_name='home-outline'
                    title='Home'
                    onPress={()=>{navigation.navigate('assignment')}}
                />
                <DrawerComponent
                    icon_name='account-circle-outline'
                    title='Profile'
                    onPress={()=>{navigation.navigate('profile')}}
                />
                <DrawerComponent
                    icon_name='clipboard-text-outline'
                    title='Assignments'
                    onPress={()=>{navigation.navigate('assignment')}}
                />
                <DrawerComponent
                    icon_name='calendar-clock'
                    title='Timesheet'
                    onPress={()=>{navigation.navigate('timesheet')}}
                />
                <DrawerComponent
                    icon_name='shield-key-outline'
                    title='Claims'
                    onPress={()=>{navigation.navigate('assignment')}}
                />
                <DrawerComponent
                    icon_name='export'
                    title='Leave'
                    onPress={()=>{navigation.navigate('assignment')}}
                />
                <DrawerComponent
                    icon_name='calendar-month'
                    title='Roster'
                    onPress={()=>{navigation.navigate('assignment')}}
                />
                <DrawerComponent
                    icon_name='clipboard-text-multiple-outline'
                    title='Payslip'
                    onPress={()=>{navigation.navigate('assignment')}}
                />
                <DrawerComponent
                    icon_name='cog-outline'
                    title='Settings'
                    onPress={()=>{navigation.navigate('assignment')}}
                />
                
            </DrawerContentScrollView>
            <TouchableOpacity style={styles.bottom}>
                <DrawerItem
                    icon={() => (
                        <Icon
                            // name='exit-to-app'
                            color={constantColor.primary}
                            size={30}
                        />
                    )}
                    label='LOGOUT'
                    labelStyle={{ fontSize: 20, color: constantColor.red }}
                    onPress={() => { dispatch(logoutController()) }}
                />
            </TouchableOpacity>

        </View>
    )
}

export default DrawerMainContent

const styles = StyleSheet.create({
    bottom: {
        marginBottom: 25,
    }
})
