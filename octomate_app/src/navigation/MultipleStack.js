import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import { constantColor } from '../view/constants/constantColor';
import PhotoScreen from '../view/screens/PhotoScreen';
import ProfileScreen from '../view/screens/ProfileScreen';
import TimeInScreen from '../view/screens/TimeInScreen';
import TimeSheetScreen from '../view/screens/TimeSheetScreen';

const Stack = createStackNavigator();

const headerStyle = {
    headerStyle: {
        backgroundColor: constantColor.primary,
        elevation: 0,
        shadowOpacity: 0,
    },
    headerTintColor: constantColor.white
}
export const ProfileStack = () => {
    return (
        <Stack.Navigator initialRouteName='profile' screenOptions={headerStyle}>
            <Stack.Screen name='profile' component={ProfileScreen} />
            <Stack.Screen name='photo' component={PhotoScreen} />
            <Stack.Screen name='timeIn' component={TimeInScreen} options={{ title: "Time In" }} />
        </Stack.Navigator>
    )
}

export const TimesheetStack = () => {
    return (
        <Stack.Navigator initialRouteName='timesheet' screenOptions={headerStyle}>
            <Stack.Screen name='timesheet' component={TimeSheetScreen} options={{ title: 'Timesheet' }} />
        </Stack.Navigator>
    )
}