import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { registerController } from '../action/loginAction';
import { getKey } from '../action/storeKey';
import { constantColor } from "../view/constants/constantColor";
import LoadingComponent from '../view/constants/loadingComponent';
import AssignmentScreen from '../view/screens/AssignmentScreen';
import LoginScreen from '../view/screens/LoginScreen';
import DrawerMainContent from './DrawerMainContent';
import { ProfileStack, TimesheetStack } from './MultipleStack';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

const MainNavigation = () => {
    const dispatch = useDispatch();
    const { isLoading, userToken } = useSelector((state) => ({
        isLoading: state.login.isLoading,
        userToken: state.login.userToken
    }))

    // useEffect(() => {
    //     async function userTokenStorage() {
    //         let userToken;
    //         try {
    //             userToken = getKey('token')
    //         } catch (error) {
    //             console.log(error)
    //         }
    //     }
    //     dispatch(registerController(userToken))
    // }, [])

    if (isLoading) { return <LoadingComponent /> }

    const headerStyle = {
        headerStyle: {
            backgroundColor: constantColor.primary,
            elevation: 0,
            shadowOpacity: 0
        },
        headerTintColor: constantColor.white
    }

    return (
        <NavigationContainer>
            {
                userToken !== null ?
                    <Drawer.Navigator initialRouteName='profileStack' drawerContent={props => <DrawerMainContent {...props} />} >
                        <Drawer.Screen name='profileStack' component={ProfileStack} />
                        <Drawer.Screen name="assignment" component={AssignmentScreen}/>
                        <Drawer.Screen name='timesheet' component={TimesheetStack} options={{ title: "Timesheet" }} />
                    </Drawer.Navigator>
                    :
                    <Stack.Navigator initialRouteName='login' screenOptions={headerStyle} >
                        <Stack.Screen name='login' component={LoginScreen} options={{ title: "" }} />
                    </Stack.Navigator>
            }
        </NavigationContainer>
    )
}

export default MainNavigation
