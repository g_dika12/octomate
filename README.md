OCTOMATE-APP

Features
1. Login Screen,
2. Drawer Navigator,
3. Logout,
4. Profile Screen, 
5. Timesheet Screen

Setup
You can simply cloning this octomate-app by typing 
'git clone https://gitlab.com/g_dika12/octomate.git' or 
'git clone git@gitlab.com:g_dika12/octomate.git'
The project is in branch master.


Project Installation
I am using 'npm' to install this project, octomate-app.
In terminal, we have to connect to the project directory by typing 

1. 'cd octomate' 
2. 'cd octomate-app', 
3. 'npm install',
4. 'cd ios && pod install && cd ..'
5. 'npx react-native run-android', or
6. 'npx react-native run-ios',
    
Detailed information about 'Run in Device' you could found :
'https://reactnative.dev/docs/running-on-device'; 

